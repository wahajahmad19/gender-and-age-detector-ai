import argparse
import math
import cv2

def highlightingFace(nnet, iFrame, threshold = 0.71):
    iFrameOpencvDnn = iFrame.copy()
    iFrameWidth = iFrameOpencvDnn.shape[1]
    iFrameHeight = iFrameOpencvDnn.shape[0]
    pixel = cv2.dnn.blobFromImage(iFrameOpencvDnn, 1.0, (300, 300), [104, 117, 123], True, False)

    nnet.setInput(pixel)
    detection = nnet.forward()
    faceBox = []
    for i in range(detection.shape[2]):
        strength = detection[0,0,i,2]
        if strength > threshold:
            x1 = int(detection[0,0,i,3] * iFrameWidth)
            x2 = int(detection[0,0,i,5] * iFrameWidth)
            y1 = int(detection[0,0,i,4] * iFrameHeight)
            y2 = int(detection[0,0,i,6] * iFrameHeight)
            faceBox.append([x1, y1, x2, y2])
            cv2.rectangle(iFrameOpencvDnn, (x1,y1), (x2,y2), (0,255,0), int(round(iFrameHeight / 150)), 8)
    return iFrameOpencvDnn,faceBox


parser = argparse.ArgumentParser()
parser.add_argument('-img')

args = parser.parse_args()

faceProtoType = "faceDetector1.pbtxt"
faceModelType = "faceDetector2.pb"
genderProtoType = "gender1.prototxt"
genderModelType = "gender2.caffemodel"
ageProtoType = "age1.prototxt"
ageModelType = "age2.caffemodel"

meanValue = (78.4263377603, 87.7689143744, 114.895847746)
ageRange = ['(0-3)', '(4-7)', '(8-11)', '(12-19)', '(20-30)', '(31-42)', '(43-50)', '(50-80)', '(81-100)']
genderType = ['MALE','FEMALE']

faceDNN = cv2.dnn.readNet(faceModelType,faceProtoType)
ageDNN = cv2.dnn.readNet(ageModelType,ageProtoType)
genderDNN = cv2.dnn.readNet(genderModelType,genderProtoType)

video = cv2.VideoCapture(args.img if args.img else 0)
padding = 21

while cv2.waitKey(1)<0:
    hasFrame, frame = video.read()

    if not hasFrame:
        cv2.waitKey()
        break

    imageResult,faceBoxs = highlightingFace(faceDNN,frame)
    if not faceBoxs:
        print("Image without Face. Use Face Images.")

    for faceBox in faceBoxs:
        faces = frame[max(0,faceBox[1] - padding):
                   min(faceBox[3] + padding, frame.shape[0] - 1), max(0, faceBox[0] - padding)
                   :min(faceBox[2] + padding, frame.shape[1] - 1)]

        blob = cv2.dnn.blobFromImage(faces, 1.0, (227,227), meanValue, swapRB = False)
        genderDNN.setInput(blob)
        genderDef = genderDNN.forward()
        gender = genderType[genderDef[0].argmax()]
        print(f'Gender_Type:: {gender}.')

        ageDNN.setInput(blob)
        ageDef = ageDNN.forward()
        age = ageRange[ageDef[0].argmax()]
        print(f'Age_Range:: {age[1:-1]} Years.')

        cv2.putText(imageResult, f'{gender}, {age}', (faceBox[0], faceBox[1]-10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0,255,255), 2, cv2.LINE_AA)
        cv2.imshow("Detection of Faces Age_Range and Gender_Type.", imageResult)